#!/usr/bin/python
import struct
import zlib

try:
	from cStringIO import StringIO
except ImportError:
	from StringIO import StringIO

MACBINARY_III_HEADER = (
	">"		# Big-endian as per Motorola standard
	"b"		# MacBinary I version number
	"b"		# Filename length
	"63s"	# 63-byte filename buffer
	"4s"	# File type code
	"4s"	# File creator code
	"b"		# Original Finder flags
	"b"		# Zero-padding
	"h"		# Icon's vertical position in window
	"h"		# Icon's horizontal position in window
	"h"		# Window or folder ID
	"b"		# "Protected" flag
	"b"		# Zero-padding
	"i"		# Data-fork length
	"i"		# Resource-fork length
	"i"		# File's creation date
	"i"		# File's last-modified date
	"h"		# Length of Get Info comment
	"b"		# More Finder flags
	"4s"	# MacBinary magic value
	"b"		# fxInfo.fdScript value
	"b"		# Extended Finder flags
	"8s"	# Zero-padding
	"i"		# Uncompressed size
	"h"		# Secondary header length
	"b"		# MacBinary version this file was created with
	"b"		# MacBinary version required to extract this file
	"i"		# Header CRC
)

MACBINARY_III_HEADER_SIZE = struct.calcsize(MACBINARY_III_HEADER)

# MacBinary III files store this in the magic field.
MACBINARY_III_MAGIC = "mBIN"

# MacBinary II files store this in the 'minimum version required to extract
# this file' field.
MACBINARY_II_VERSION = 129

assert MACBINARY_III_HEADER_SIZE == 128, (
		"Header should be 128 bytes, not %r" % MACBINARY_III_HEADER_SIZE
	)

# Since we're about to shadow this later on, let's save a reference to the real
# open().
real_open = open

class MacBinaryFailure(Exception):
	pass

class MacBinaryFile(object):

	def __init__(self, filename):
		self.handle = real_open(filename, 'rb')

		self._read_header()

	def _align_offset(self, offset):
		"""
		Rounds the given offset up to the nearest 128 byte-boundary.
		"""
		div, mod = divmod(offset, 128)
		if mod:
			return (1+div) * 128
		else:
			return offset

	def _read_header(self):
		raw_header = self.handle.read(struct.calcsize(MACBINARY_III_HEADER))

		(
			version_check_1,
			name_length,
			raw_name,
			self.type,
			self.creator,
			self._flags1,
			version_check_2,
			self._y_pos,
			self._x_pos,
			self._container_id,
			self._flags2,
			version_check_3,
			self.data_length,
			self.rsrc_length,
			self._ctime,
			self._mtime,
			comment_length,
			self._flags3,
			self._magic,
			self._script,
			self._flags4,
			_,
			self._unpacked_size,
			extended_header_length,
			self._writer_version,
			reader_version,
			header_crc,
		) = struct.unpack(MACBINARY_III_HEADER, raw_header)

		# Version-checking
		if self._magic == MACBINARY_III_MAGIC:
			# MacBinary III introduced the handy magic field.
			self._macbinary_version = 3
		elif version_check_1 == 0 and version_check_2 == 0:
			# Might be MacBinary I or II.
			if zlib.crc32(raw_header) == header_crc:
				# MacBinary II introduced the CRC.
				self._macbinary_version = 2
				if reader_version > MACBINARY_II_VERSION:
					raise MacBinaryFailure("Need MacBinary version %d "
							"or higher to read this" % (reader_version,))
			elif version_check_3 == 0:
				self._macbinary_version = 1
		else:
			raise MacBinaryFailure("Not a MacBinary file")

		# Sanity checking
		if name_length > 63:
			raise MacBinaryFailure("name_length field should be less than 63")
		if self.data_length > 0x7FFFFF:
			raise MacBinaryFailure("data_length should be less than 0x7FFFFF")
		if self.rsrc_length > 0x7FFFFF:
			raise MacBinaryFailure("rsrc_length should be less than 0x7FFFFF")

		self.name = raw_name[:name_length]

		data_start = self._align_offset(MACBINARY_III_HEADER_SIZE
				+ extended_header_length)

		rsrc_start = self._align_offset(data_start + self.data_length)

		comment_start = self._align_offset(rsrc_start + self.rsrc_length)

		self.handle.seek(data_start)
		self.data = self.handle.read(self.data_length)
		assert len(self.data) == self.data_length

		self.handle.seek(rsrc_start)
		self.rsrc = self.handle.read(self.rsrc_length)
		assert len(self.rsrc) == self.rsrc_length

		self.handle.seek(comment_start)
		self.comment = self.handle.read(comment_length)
		assert len(self.comment) == comment_length

	def __repr__(self):
		if self._macbinary_version == 1:
			classname = "MacBinary"
		elif self._macbinary_version == 2:
			classname = "MacBinary II"
		elif self._macbinary_version == 3:
			classname = "MacBinary III"
		else:
			classname = "Unknown MacBinary"

		return "<%s file, name: %r, type: %r, creator: %r>" % (
				classname,
				self.name,
				self.type,
				self.creator,
			)

def openrf(filename, mode):
	assert "r" in mode, "Only reading is currently supported."
	mb = MacBinaryFile(filename)
	return StringIO(mb.rsrc)

def open(filename, mode):
	assert "r" in mode, "Only reading is currently supported."
	mb = MacBinaryFile(filename)
	return StringIO(mb.data)

if __name__ == "__main__":
	import sys
	handle = MacBinaryFile(sys.argv[1])
	print "Name:\t", repr(handle.name)
	print "Type:\t", repr(handle.type)
	print "Creator:\t", repr(handle.creator)
	print "Data length:\t", repr(handle.data_length)
	print "Resource length:\t", repr(handle.rsrc_length)
	print "Get Info comment:"
	print handle.comment
	print "MacBinary version:\t", handle._macbinary_version
