import os.path
import re

COMMENT_RE = re.compile("#.*")

# Certain fonts (such as Chicago) store UI glyphs in the control-character
# area. These glyphs aren't part in any standard text encoding, but are
# mentioned in the special "KEYBOARD" encoding for the ".Keyboard" font.
STANDARD_ENCODINGS = {
		0x11: [0x2318], # PLACE OF INTEREST SIGN # Command key
		0x12: [0x2713], # CHECK MARK
		0x13: [0x25C6], # BLACK DIAMOND
		0x14: [0xF8FF], # Apple logo

		# Every bitmap font includes an 'unknown glyph' glyph which our code
		# stores at code -1.
		-1: [0xFFFD],
	}

def get_encoding_path(name):
	basedir = os.path.join(os.path.dirname(__file__), "encodings")
	return os.path.join(basedir, "%s.TXT" % (name.upper()))

def strtoint(string):
	string = string.strip().lower()
	if string.startswith("0x"):
		base = 16
		digits = string[2:]
	elif string.startswith("0"):
		base = 8
		digits = string[1:]
	else:
		base = 10
		digits = string
	
	return int(digits, base)

def read_encoding(handle):
	res = {}
	for lineno, line in enumerate(handle):
		line = COMMENT_RE.sub("", line).strip()
		if line == "":
			continue

		code, codepoints = line.split("\t")
		codepoints = codepoints.split("+")

		if code == "" or codepoints == ("",):
			continue

		res[strtoint(code)] = [strtoint(cp) for cp in codepoints]

	# Add the standard UI codes.
	res.update(STANDARD_ENCODINGS)
	return res

def build_encoding(name):
	fullpath = get_encoding_path(name)
	try:
		handle = open(fullpath, 'r')
		try:
			return read_encoding(handle)
		finally:
			handle.close()
	except IOError:
		raise KeyError("Could not read data for encoding %r from %r" % (
				name,
				fullpath,
			))
		
def guess_encoding_name(family):
	if family.name == "Symbol":
		return "SYMBOL"
	
	# Until such time as we figure out how to detect the encoding of
	# a Macintosh font, default to MacRoman
	return "ROMAN"
