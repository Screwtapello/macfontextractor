"""
Classes for interpreting individual Macintosh resources.
"""
import struct

class AbstractResource(object):

	def __init__(self, fork, id, name, data):
		self.id = id
		self.name = name
		self.data = data

	def __repr__(self):
		if self.name:
			return "<Resource %d named %r, %d bytes>" % (
					self.id, self.name, len(self.data)
				)
		else:
			return "<Resource %d, %d bytes>" % (
					self.id, len(self.data)
				)


class StringResource(AbstractResource):

	def __str__(self):
		length = ord(self.data[0])
		return self.data[1:length+1]

	def __repr__(self):
		return "<StringResource %d: %r>" % (self.id, str(self))


class FontFamilyResource(AbstractResource):

	HEADER = (
			# From "Inside Macintosh: Text", figure 4-22.
			">"		# Big-endian as per Motorola standard.
			"H"		# Font-family flags
			"H"		# Font-family ID
			"H"		# First glyph in font
			"H"		# Last glyph in font
			"h"		# Font maximum ascent
			"h"		# Font maximum descent
			"h"		# Font maximum leading
			"h"		# Font maximum glyph width
			"L"		# Offset to family glyph-width table
			"L"		# Offset to family kerning table
			"L"		# Offset to style-mapping table
			"H"		# Width multiplier for 'plain' text
			"H"		# Width multiplier for 'bold' text
			"H"		# Width multiplier for 'italic' text
			"H"		# Width multiplier for 'underline' text
			"H"		# Width multiplier for 'outline' text
			"H"		# Width multiplier for 'shadow' text
			"H"		# Width multiplier for 'condense' text
			"H"		# Width multiplier for 'extend' text
			"H"		# Unused
			"L"		# Reserved for international information
			"H"		# Version
		)
	HEADER_SIZE = struct.calcsize(HEADER)
	assert HEADER_SIZE == 52

	# Flags for the 'font-family flags' field
	# Reserved			 			0x0000
	FF_HAS_GLYPH_WIDTH =			0x0001
	# Reserved						0x0002
	# ...							...
	# Reserved						0x0800
	FF_IGNORE_FRACT_ENABLE =		0x1000
	FF_USE_INTEGER_WIDTH =			0x2000
	FF_IGNORE_FRAC_WIDTH_TABLE =	0x4000
	FF_FONT_IS_MONOSPACE = 			0x8000

	# Flags for font-styles
	# From "Inside Macintosh: Text", figure 4-23.
	FS_BOLD =						0x01
	FS_ITALIC =						0x02
	FS_UNDERLINE =					0x04
	FS_OUTLINE =					0x08
	FS_SHADOW =						0x10
	FS_CONDENSE =					0x20
	FS_EXTEND =						0x40
	# Reserved						0x80
	
	def _parse_fixed_point(self, value):
		"""
		Interpret a 4.12 fixed-point fraction in a 16-bit field.
		"""
		int_part = value >> 12
		frac_part = float(value & 0xfff) / 0xfff

		return int_part + frac_part

	def __init__(self, fork, *args, **kwargs):
		super(FontFamilyResource, self).__init__(fork, *args, **kwargs)

		raw_header = self.data[:self.HEADER_SIZE]

		(self.flags, self.family_id, self.first_glyph, self.last_glyph,
				self.max_ascent, self.max_descent, self.max_leading,
				self.max_width, self.glyph_width_start, self.kerning_start,
				self.style_mapping_start, self.wm_plain, self.wm_bold,
				self.wm_italic, self.wm_underline, self.wm_outline,
				self.wm_shadow, self.wm_condense, self.wm_extend, _,
				self._international_info, self.version) = struct.unpack(
						self.HEADER, raw_header)

		assert self.id == self.family_id, (
				"Family ID %d should match resource ID %d" % (
					self.family_id, self.id
				)
			)

		assert self._international_info == 0, ("We currently only support "
				"MacRoman fonts.")

		# Some of the fields in the header are listed as 16-bit integers, but
		# they're actually fixed-point values.
		self.max_ascent   = self._parse_fixed_point(self.max_ascent)
		self.max_descent  = self._parse_fixed_point(self.max_descent)
		self.max_leading  = self._parse_fixed_point(self.max_leading)
		self.max_width    = self._parse_fixed_point(self.max_width)
		self.wm_plain     = self._parse_fixed_point(self.wm_plain)
		self.wm_bold      = self._parse_fixed_point(self.wm_bold)
		self.wm_italic    = self._parse_fixed_point(self.wm_italic)
		self.wm_underline = self._parse_fixed_point(self.wm_underline)
		self.wm_condense  = self._parse_fixed_point(self.wm_condense)
		self.wm_extend    = self._parse_fixed_point(self.wm_extend)

		curr_pos = self.HEADER_SIZE
		curr_pos = self._read_association_table(fork, curr_pos)

		# There's other stuff that might appear after the association table,
		# but we don't care about it for extracting bitmap font data.

	ASSOCIATION_TABLE_ENTRY = (
			# From "Inside Macintosh: Text", figure 4-24.
			">"		# Big-endian as per Motorola standard.
			"H"		# Font-size (in points)
			"H"		# Font style flags
			"h"		# Resource ID containing the font of this style.
		)
	ASSOCIATION_TABLE_ENTRY_SIZE = struct.calcsize(ASSOCIATION_TABLE_ENTRY)
	assert ASSOCIATION_TABLE_ENTRY_SIZE == 6

	def _read_association_table(self, fork, start):
		raw_last_entry_index = self.data[start:start+2]
		start += 2
		last_entry_index = struct.unpack(">H", raw_last_entry_index)[0]

		self.associations = {}

		while len(self.associations) < last_entry_index + 1:
			end = start + self.ASSOCIATION_TABLE_ENTRY_SIZE
			raw_entry = self.data[start:end]
			(size, style, resource_id) = struct.unpack(
					self.ASSOCIATION_TABLE_ENTRY, raw_entry)
			resource = None
			for type in 'sfnt', 'NFNT', 'FONT':
				try:
					resource = fork.get_resource(type, resource_id)
					break
				except KeyError:
					pass
			self.associations[(size, style)] = resource

			start = end

		return start

	def __repr__(self):
		if self.flags & self.FF_FONT_IS_MONOSPACE:
			desc = " (monospace)"
		else:
			desc = ""

		return "<FontFamily %d %r%s version 0x%04X, chars %d-%d>" % (
				self.id, self.name, desc, self.version, self.first_glyph,
				self.last_glyph,
			)


class BitmapFontResource(AbstractResource):

	HEADER = (
			# From "Inside Macintosh: Text", figure 4-16.
			">"		# Big-endian as per Motorola standard.
			"H"		# Font flags
			"H"		# First glyph encoding
			"H"		# Last glyph encoding
			"h"		# Maximum width (rightmost point on any glyph)
			"h"		# Maximum kerning (leftmost point on any glyph)
			"h"		# Negated descent or high word of offset to w/o table
			"h"		# Max glyph width
			"h"		# Max glyph height
			"H"		# Low word of offset to width/offset table.
			"h"		# Max ascent
			"h"		# Max descent
			"h"		# Leading
			"H"		# Bit image row width
		)
	HEADER_SIZE = struct.calcsize(HEADER)
	assert HEADER_SIZE == 26

	# Flag values for the flags field in the header.
	FF_FONT_HAS_IMAGE_HEIGHT_TABLE =	0x0001
	FF_FONT_HAS_GLYPH_WIDTH_TABLE =		0x0002
	FF_PIXEL_FORMAT_MASK =				0x000C
	FF_PIXEL_FORMAT_1B =				0x0000
	FF_PIXEL_FORMAT_2B =				0x0004
	FF_PIXEL_FORMAT_4B =				0x0008
	FF_PIXEL_FORMAT_8B =				0x000C
	# Reserved							0x0010
	# ...								...
	# Reserved							0x0040
	FF_FONT_HAS_COLOR_TABLE =			0x0080
	FF_FONT_IS_SYNTHETIC =				0x0100
	FF_FONT_HAS_NON_BLACK_PIXELS =		0x0200
	# Reserved							0x0400
	# ...								...
	# Reserved							0x1000
	FF_FONT_IS_MONOSPACE =				0x2000
	FF_PROHIBIT_SYNTHETIC =				0x4000
	# Reserved							0x8000

	def __init__(self, *args, **kwargs):
		super(BitmapFontResource, self).__init__(*args, **kwargs)

		raw_header = self.data[:self.HEADER_SIZE]

		(self.flags, self.first_glyph, self.last_glyph, self.max_width,
				self.max_kerning, self.neg_descent, self.max_glyph_width,
				self.max_glyph_height, self.wo_table_start, self.max_ascent,
				self.max_descent, self.leading, self.row_width_words
			) = struct.unpack(self.HEADER, raw_header)

		# If neg_descent is positive, it represents the upper 16 bits of the
		# offset to the width/offset table, otherwise it's a place to cache the
		# result of (self.max_descent * -1).
		# See page 4-71 of "Inside # Macintosh: Text".
		if self.neg_descent > 0:
			self.wo_table_start += (self.neg_descent << 16)

		# There's an extra 'no glyph' glyph after all the other glyphs.
		self.glyph_count = self.last_glyph - self.first_glyph + 1

		# This list contains a long integer for every row of the font bitmap.
		self.bitmap_data = []

		# Each of these dicts is indexed by a character code and stores metrics
		# about the glyph in question.
		self.bitmap_offsets = {}
		self.glyph_offsets = {}
		self.glyph_widths = {}
		self.glyph_heights = {}

		next_start = self._read_bitmap(self.HEADER_SIZE)
		next_start = self._read_bitmap_offset_table(next_start)
		next_start = self._read_width_offset_table(next_start)
		next_start = self._read_glyph_width_table(next_start)
		self._read_image_height_table(next_start)

	def _read_bitmap(self, next_start):

		assert (self.flags & self.FF_PIXEL_FORMAT_MASK ==
				self.FF_PIXEL_FORMAT_1B), "Only 1-bit fonts supported"
		assert (self.flags & self.FF_FONT_HAS_COLOR_TABLE == 0), (
				"Only monochrome fonts supported.")
		assert (self.flags & self.FF_FONT_HAS_NON_BLACK_PIXELS == 0), (
				"Only monochrome fonts supported.")

		row_width_bytes = self.row_width_words * 2

		while len(self.bitmap_data) < self.max_glyph_height:
			# Bitmap begins just after the header
			start = next_start + (
					len(self.bitmap_data) * row_width_bytes)
			end = start + row_width_bytes
			raw_row_data = self.data[start:end]

			# Turn the bytes into a long integer
			self.bitmap_data.append(
					reduce(
						lambda cur, newbyte: (cur << 8) + ord(newbyte),
						raw_row_data,
						0L,
					)
				)

		return end

	def _read_table(self, table_start, format, count):
		"""
		Reads an array of structs from the given position.
		"""
		res = []
		entry_size = struct.calcsize(format)
		entry_start = table_start
		while entry_start < table_start + (count+1) * entry_size:
			entry_end = entry_start + entry_size
			res.append(struct.unpack(format, self.data[entry_start:entry_end]))
			entry_start = entry_end

		return res, entry_start

	def _read_bitmap_offset_table(self, next_start):
		raw_bitmap_offsets, next_start = self._read_table(
				next_start,
				">H",
				# The bitmap offset table has an extra entry at the end to mark
				# the right-hand-side of the final glyph.
				self.glyph_count + 1,
			)

		# For each character, we really want a start and end position, so let's
		# unpack what _read_table returned.
		for i in range(len(raw_bitmap_offsets)-2):
			colstart = raw_bitmap_offsets[i][0]
			colend = raw_bitmap_offsets[i+1][0]
			self.bitmap_offsets[i+self.first_glyph] = (colstart, colend)

		# Add the "no glyph" glyph to the map.
		self.bitmap_offsets[-1] = (
				raw_bitmap_offsets[-2][0],
				raw_bitmap_offsets[-1][0],
			)

		return next_start

	def _read_width_offset_table(self, next_start):
		# Read the glyph width/offset table.
		width_offsets, next_start = self._read_table(
				next_start,
				">BB",
				self.glyph_count,
			)

		for i in range(len(width_offsets)-1):
			offset, width = width_offsets[i]
			if offset == 255 and width == 255:
				# No metrics for this char.
				continue
			self.glyph_offsets[i+self.first_glyph] = offset
			self.glyph_widths[i+self.first_glyph] = width

		# Add the "no glyph" glyph to the maps.
		self.glyph_offsets[-1] = width_offsets[-1][0]
		self.glyph_widths[-1] = width_offsets[-1][1]

		return next_start

	def _read_glyph_width_table(self, next_start):
		if not self.flags & self.FF_FONT_HAS_GLYPH_WIDTH_TABLE:
			return next_start

		# I don't have any fonts that include a glyph width table, so
		# I don't know if this is all correct.
		real_glyph_widths, next_start = self._read_table(
				next_start,
				">BB",
				self.glyph_count,
			)
		for i, (width, frac_width) in enumerate(real_glyph_widths):
			# The glyph-widths table, if present, overrides the values in
			# the width/offset table.
			self.glyph_widths[i+self.first_glyph] = (
					width + float(frac_width)/255
				)

		return next_start

	def _read_image_height_table(self, next_start):
		if not self.flags & self.FF_FONT_HAS_IMAGE_HEIGHT_TABLE:
			return next_start

		raw_glyph_heights, _ = self._read_table(
				# Although I can't justify it from any documentation, all
				# the sample fonts I have with an Image Height table have
				# two bytes of padding before the table begins. There's
				# also two bytes of padding after, but since the Image
				# Height table is the last thing in the resource, we can
				# ignore whatever comes after it.
				next_start+2,
				">BB",
				self.glyph_count,
			)
		for i in range(len(raw_glyph_heights)-1):
			self.glyph_heights[i+self.first_glyph] = raw_glyph_heights[i]
		self.glyph_heights[-1] = raw_glyph_heights[-1]

		return next_start

	def __repr__(self):
		return "<BitmapFontResource chars %d-%d, %dx%d pixels>" % (
				self.first_glyph, self.last_glyph, self.max_glyph_width,
				self.max_glyph_height,
			)

	def _extract_glyph_bitmap(self, colstart, colend, rowstart, rowend):
		glyph_width_bits = colend - colstart
		row_width_bits = self.row_width_words * 16

		# Build a mask to extract just the bits of the glyph we want.
		mask = 0
		for i in range(glyph_width_bits):
			mask = (mask << 1) + 1

		# The LSB is at the far-right of the bitmap, so we'll have to shift our
		# glyph across so we can get at them.
		shift = row_width_bits - colend

		return [
				(row >> shift) & mask
				for row in self.bitmap_data[rowstart:rowend]
			]

	def get_glyph_codes(self):
		"""
		List all the glyph codes defined in this font.
		"""
		return self.glyph_offsets.keys()

	def get_glyph_data(self, code):
		"""
		Return the bitmap and metrics for the glyph with the given code.

		Returns a (bitmap, bitmap-width, top, offset, glyph-width) tuple.
		"""
		# The width/offset table is the only table that has a "no such
		# character" sentinel value, and the widths might be overwritten by the
		# glyph width table, so the offset table is the only way to guess
		# which characters actually exist.
		if code not in self.glyph_offsets:
			return None

		offset = self.glyph_offsets[code]
		width = self.glyph_widths[code]

		# There seems to be a lot of useless empty glyphs with zero widths, so
		# let's throw those out too.
		if width == 0:
			return None

		colstart, colend = self.bitmap_offsets[code]
		rowstart, rowcount = self.glyph_heights.get(code,
				(0, self.max_glyph_height))
		rowend = rowstart + rowcount
		bitmap = self._extract_glyph_bitmap(colstart, colend, rowstart, rowend)

		return (bitmap, colend-colstart, rowstart, offset, width)


class OriginalBitmapFontResource(BitmapFontResource):

	def __repr__(self):
		family_id = self.id >> 7
		size = self.id & 0x7f
		if self.name:
			desc = "named %r " % self.name
		else:
			desc = ""

		return "<OriginalBitmapFont %d family %d %s%dpt, %d bytes>" % (
				self.id, family_id, desc, size, len(self.data),
			)

RESOURCE_HANDLERS = {
		'STR ': StringResource,
		'FOND': FontFamilyResource,
		'NFNT': BitmapFontResource,
		'FONT': OriginalBitmapFontResource,
	}

def build_handler_for_type(type, fork, id, name, data):
	cls = RESOURCE_HANDLERS.get(type, AbstractResource)
	return cls(fork, id, name, data)
