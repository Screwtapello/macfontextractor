import os.path
from bdflib.model import Font, Glyph
from bdflib.writer import write_bdf
from macfontextractor import macbinary, resourcefork, resource_handlers
from macfontextractor.encodings import guess_encoding_name, build_encoding

def human_style_name(rsrc, style):
	name_parts = []
	if style & rsrc.FS_BOLD: name_parts.append("Bold")
	if style & rsrc.FS_ITALIC: name_parts.append("Italic")
	if style & rsrc.FS_UNDERLINE: name_parts.append("Underline")
	if style & rsrc.FS_OUTLINE: name_parts.append("Outline")
	if style & rsrc.FS_SHADOW: name_parts.append("Shadow")
	if style & rsrc.FS_CONDENSE: name_parts.append("Condensed")
	if style & rsrc.FS_EXTEND: name_parts.append("Extended")

	# If the name has no parts, default to "Regular"
	return " ".join(name_parts) or "Regular"

def build_properties_from_font_data(family, style, size, font):
	res = {}

	res["FAMILY_NAME"] = family.name
	res["FONTNAME_REGISTRY"] = ""
	res["FOUNDRY"] = "mac"

	if style & family.FS_BOLD:
		res["WEIGHT_NAME"] = "Bold"

	if style & family.FS_ITALIC:
		res["SLANT"] = "i"
	else:
		res["SLANT"] = "r"

	if style & family.FS_CONDENSE:
		res["SETWIDTH_NAME"] = "Condensed"
	elif style & family.FS_EXTEND:
		res["SETWIDTH_NAME"] = "Extended"

	addStyleNames = []
	if style & family.FS_UNDERLINE: addStyleNames.append("Underline")
	if style & family.FS_OUTLINE:   addStyleNames.append("Outline")
	if style & family.FS_SHADOW:    addStyleNames.append("Shadow")
	res["ADD_STYLE_NAME"] = " ".join(addStyleNames)

	if family.flags & family.FF_FONT_IS_MONOSPACE:
		res["SPACING"] = "M"
	else:
		res["SPACING"] = "P"

	return res

def iter_bitmap_font_resources(resource_fork):
	for family in resource_fork.get_resources_by_type('FOND'):
		for ((size, style), font) in sorted(family.associations.items()):
			if not isinstance(font, resource_handlers.BitmapFontResource):
				print "Ignoring %r" % font
				continue
			human_name = "%s %s %dpt" % (
					family.name,
					human_style_name(family, style),
					size
				)
			yield (family, style, size, font, human_name)

def convert_glyph_data(bitmap, bitwidth):
	bytewidth, remainder = divmod(bitwidth, 8)
	if remainder:
		# We have to shift the bitmap left so that the bitmap is left-aligned
		# in an integer number of bytes.
		bytewidth += 1
		offset = 8 - remainder
		bitmap = [row << offset for row in bitmap]

	# Convert to hex digits required by BDF
	bitmap = ["%0*X" % (bytewidth*2, row) for row in bitmap]
	return bitmap

def convert_font(family, style, size, font, human_name):

	encoding_name = guess_encoding_name(family)
	encoding = build_encoding(encoding_name)

	# All Mac bitmap fonts are nominally 72dpi.
	res = Font(human_name, size, 72, 72)

	properties = build_properties_from_font_data(family, style, size, font)

	for key, value in properties.items():
		res[key] = value

	# Tell the font we're going to use Unicode codepoints.
	res["CHARSET_REGISTRY"] = "iso10646"
	res["CHARSET_ENCODING"] = "1"

	for code in font.get_glyph_codes():
		glyph_data = font.get_glyph_data(code)
		if not glyph_data:
			continue

		# Figure out the Unicode code-point associated with this font.
		if code not in encoding:
			print "Don't know how to decode code %r, skipping" % code
			continue

		# Sometimes multiple codes represent the same Unicode codepoint, but
		# are distinct in their source encoding. For example, the Symbol font
		# contains both serif and sans-serif versions of U+00AE. In these
		# cases, the list of codepoints is suffixed with a 'variant tag'
		# character (see CORPCHAR.TXT for details). We can strip it off with no
		# loss of generality.
		codepoints = [cp for cp in encoding[code]
				if cp not in range(0xF870, 0xF880)
			]
		if len(codepoints) > 1:
			print "Code %d maps to multiple codepoints, skipping: %r" % (
					code, " ".join(["U+%04X" % cp for cp in codepoints]))
			continue

		codepoint = codepoints[0]

		if codepoint in res:
			print "Codepoint U+%04X already used, skipping." % codepoint
			continue

		# Convert Macintosh font metrics to BDF font metrics
		bitmap, bitwidth, rowstart, offset, width = glyph_data

		bbX = font.max_kerning + offset
		bbW = bitwidth
		bbY = (
				(font.max_glyph_height - font.max_descent)
				-
				(rowstart + len(bitmap))
			)
		bbH = len(bitmap)
		advance = width

		g = res.new_glyph_from_data(
				"uni%04X" % codepoint,
				convert_glyph_data(bitmap, bitwidth),
				bbX, bbY, bbW, bbH,
				advance,
				codepoint,
			)

	return res

def convert_font_file(macbinary_file, output_dir):
	handle = macbinary.openrf(macbinary_file, 'rb')
	rf = resourcefork.ResourceFork(handle)

	for family, style, size, font, human_name in \
			iter_bitmap_font_resources(rf):
		print "Processing %s..." % human_name

		output_name = os.path.join(output_dir,
				"%s.bdf" % (human_name.replace(" ", "-"),)
			)
		bdf_font = convert_font(family, style, size, font, human_name)

		output = open(output_name, 'w')
		write_bdf(bdf_font, output)
		output.close()

	handle.close()
