#!/usr/bin/python
import sys
from macfontextractor import macbinary, resourcefork, resource_handlers

handle = macbinary.openrf(sys.argv[1], 'rb')
rf = resourcefork.ResourceFork(handle)

def render_bits(n, width):
	masks = [1<<x for x in reversed(range(width))]
	res = []
	for mask in masks:
		if n & mask:
			res.append("#")
		else:
			res.append(".")
	return "".join(res)

for rsrc in rf.get_resources_by_type('FOND'):
	print "FOND id %d named %r:" % (rsrc.id, rsrc.name)

	print " Max Ascent:", rsrc.max_ascent
	print " Max Descent:", rsrc.max_descent
	print " Max Leading:", rsrc.max_leading
	print " Max Width:", rsrc.max_width
	print " I18n data:", rsrc._international_info
	print " Glyph range: %d-%d" % (rsrc.first_glyph, rsrc.last_glyph)

	for (size, style) in sorted(rsrc.associations.keys()):
		style_names = []
		if style & rsrc.FS_BOLD: style_names.append("Bold")
		if style & rsrc.FS_ITALIC: style_names.append("Italic")
		if style & rsrc.FS_UNDERLINE: style_names.append("Underline")
		if style & rsrc.FS_OUTLINE: style_names.append("Outline")
		if style & rsrc.FS_SHADOW: style_names.append("Shadow")
		if style & rsrc.FS_CONDENSE: style_names.append("Condensed")
		if style & rsrc.FS_EXTEND: style_names.append("Extended")

		style_desc = " ".join(style_names)
		if not style_desc: style_desc = "Regular"

		print " %s %s %dpt" % (rsrc.name, style_desc, size)

		font = rsrc.associations[(size, style)]
		if isinstance(font, resource_handlers.BitmapFontResource):
			print "  Flags: 0x%04X" % font.flags
			print "  Has glyph width table: %s" % bool(
					font.flags & font.FF_FONT_HAS_GLYPH_WIDTH_TABLE)
			print "  Has image height table: %s" % bool(
					font.flags & font.FF_FONT_HAS_IMAGE_HEIGHT_TABLE)
			print "  Glyph range: %d-%d" % (font.first_glyph, font.last_glyph)
			print "  Max Width: %dpx" % font.max_width
			print "  Max Kerning: %dpx" % font.max_kerning
			print "  Neg Descent: %d" % font.neg_descent
			print "  Max Glyph Width: %d" % font.max_glyph_width
			print "  Max Glyph Height: %d" % font.max_glyph_height
			print "  Width/offset start: 0x%04X" % font.wo_table_start
			print "  Max Ascent: %dpx" % font.max_ascent
			print "  Max Descent: %dpx" % font.max_descent
			print "  Max Leading: %dpx" % font.leading
			print "  Row Width: %d words" % font.row_width_words
			print "  Glyph count: %d" % len(font.bitmap_offsets)
			print "  Glyph offsets: %d" % len(font.glyph_offsets)
			print "  Glyph widths: %d" % len(font.glyph_widths)
			print "  Glyph heights: %d" % len(font.glyph_heights)

			last_col = font.bitmap_offsets[-1][1]
			row_width_bits = font.row_width_words * 16
			print "  Slack bits in bitmap: %d" % (row_width_bits - last_col)

			print "  Glyph data:"
			for code in font.get_glyph_codes():
				glyph_data = font.get_glyph_data(code)
				if not glyph_data:
					continue

				bitmap, bitwidth, rowstart, offset, width = glyph_data

				print "   Char %d, top %s, left %s, width %s, advance %s" % (
						code, rowstart, offset + font.max_kerning, bitwidth,
						width,
					)

				for row in bitmap:
					print "    %s" % render_bits(row, bitwidth)

		else:
			print "  %r" % (rsrc.associations[(size, style)],)
